use strict;
use warnings;

use Test::More;
use Mojo::JSON qw(decode_json);

require 't/lib/utils.pl';

my $class  = 'Local::Fetcher';
my $method = 'get_webpage';

package Mock::UA {
	sub AUTOLOAD { __PACKAGE__ }
	sub can { 1 }
	sub body { q({ "meta": "1"}) }
	}

subtest setup => sub {
	use_ok( $class ) or BAIL_OUT( "$class did not compile" );
	can_ok( $class, $method );
	};

subtest mock_ua => sub {
	can_ok( 'Mock::UA', qw(get res body) );
	};

subtest medallions => sub {
	my $url = 'https://nycopendata.socrata.com/api/views/rhe8-mgbb/rows.json?accessType=DOWNLOAD';
	$class->set_ua('Mock::UA'); 
	check_a_url( $url, $class, $method );
	};

done_testing();
